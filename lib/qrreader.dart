import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:last_qr_scanner/last_qr_scanner.dart';

class QrReaderScreen extends StatefulWidget {
  QrReaderScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<QrReaderScreen> {
  static AudioCache player = new AudioCache();
  var lastcode;
  bool bip, copy, open;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = ""; 
  var controller;
  bool lights = false;
  bool is_resumed = true;
  bool is_reading = false;
  int count_codes = 0;

  @override
  void initState() {
    lastcode = '';
    lights = false;
    super.initState();
    handleAppLifecycleState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  handleAppLifecycleState() {
    AppLifecycleState _lastLifecyleState;
    SystemChannels.lifecycle.setMessageHandler((msg) {

     print('SystemChannels> $msg');

        switch (msg) {
          case "AppLifecycleState.paused":
            _lastLifecyleState = AppLifecycleState.paused;
            break;
          case "AppLifecycleState.inactive":
            _lastLifecyleState = AppLifecycleState.inactive;
            break;
          case "AppLifecycleState.resumed":
            _lastLifecyleState = AppLifecycleState.resumed;
            print('resumed ? ' + is_resumed.toString());
            if (!is_resumed) {
              this.controller.resumeScanner();
              is_resumed = true;
              setState(() {
                is_resumed = true;
              });
            }
            break;
          case "AppLifecycleState.suspending":
            _lastLifecyleState = AppLifecycleState.suspending;
            break;
          default:
        }
    });
  }

  processData( code ) async {   
    if (bip) {
      const bip_sound = "sounds/bip.mp3";
      player.play(bip_sound);
    }

    if (copy) {
      if (lastcode != code) {
        Clipboard.setData(new ClipboardData(text: code));
        final snackBar = SnackBar(
            content: Text('Se copio al portapapeles'),
          );
          Scaffold.of(context).showSnackBar(snackBar);
      }
    }

    if (open) {
      if (is_resumed) {
        if (await canLaunch(code)) {
          setState(() {
            lastcode = code;
            is_resumed = false;
          });
          await launch(code);
        } else {
          final snackBar = SnackBar(
            content: Text('No se puede abrir la url'),
          );
          Scaffold.of(context).showSnackBar(snackBar);
        }
      }
    } else {
      // wait 2 secods before scan again
      setState(() {
        lastcode = code;
      });
      await new Future.delayed(const Duration(seconds: 2), );
      this.controller.resumeScanner();
      // this.controller.resumeScanner();
    }

    await addtoHistory(code);
    
  }
  
  foundQR(code) async {

    await this.controller.pauseScanner();
    print('codigo ' + code);
    count_codes = count_codes + 1;
    print('num_code ' + count_codes.toString());

    print('llama process');
    await processData(code);
    
    // count_codes = 0;

  }

  addtoHistory(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> history = prefs.getStringList('history');
    if (history == null ) {
      history = new List<String>();
      history.add(value);
    } else {
      if (history.length < 50) {
        history.add(value);
      } else {
        history.removeAt(0);
        history.add(value);
      }
    }
    await prefs.setStringList('history', history);
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    final channel = controller.channel;
    controller.init(qrKey);
    channel.setMethodCallHandler((MethodCall call) async {
      switch (call.method) {
        case "onRecognizeQR":
          dynamic arguments = call.arguments;
          foundQR(arguments.toString());
          setState(() {
            qrText = arguments.toString();
          });
      }
    });
  }

  getImage() {
    if (lights == true) {
      return Image(image: AssetImage('assets/images/light_on.png'),
                height: 30.0,
              );
    } else {
      return Image(image: AssetImage('assets/images/light_off.png'),
                height: 30.0,
              );
    }
  }

  switchlights() async {
    setState(() {
      lights = !lights;
    });
    this.controller.toggleTorch();
  }

  getConfigData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _bip = prefs.getBool('bip') == null ? true : prefs.getBool('bip');
    bool _copy = prefs.getBool('copy') == null ? true : prefs.getBool('copy');
    bool _open = prefs.getBool('open') == null ? true : prefs.getBool('open');
    setState(() {
      bip =_bip;
      copy =_copy;
      open =_open;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 70.0;
    getConfigData();
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF211f20),
          title: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Escanea el QR'),
                IconButton(
                  icon: getImage(),
                  onPressed: () { switchlights(); },
                ),
              ],
            ),
          ),
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              RootScaffold.openDrawer(context);
            },
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Color(0xFF000000)
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 0.0, top: 0.0),
                  child: Image(
                    image: NetworkImage('http://www.qrperu.org/lu.jpg'),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1.0,
                      color: Color(0xFF717171),
                    ),
                  ),
                  width: width,
                  height: width,
                  child: LastQrScannerPreview(
                    key: qrKey,
                    onQRViewCreated: _onQRViewCreated,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 0.0, top: 0.0),
                  child: Image(
                    image: NetworkImage('http://www.qrperu.org/ld.jpg'),
                  ),
                ),
              ],
            ),
          ),
        )
      );
  }
}



class RootScaffold {
  static openDrawer(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    scaffoldState.openDrawer();
  }

  static ScaffoldState of(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    return scaffoldState;
  }
}
