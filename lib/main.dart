import 'package:flutter/material.dart';
import './configurations.dart';
import './qrreader.dart';
import './drawer.dart';
import './history.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => QrReaderScreen(),
        '/config': (context) => ConfigurationsScreen(),
        '/history': (context) => HistoryScreen(),
      },
      builder: (context, child) {
        return Scaffold(
          drawer: CustomDrawer(navigator: (child.key as GlobalKey<NavigatorState>)),
          body: child,
        );
      },
    );
  }
}